<?
namespace Astartsky;

use Igorw\Silex\ConfigDriver;
use Igorw\Silex\YamlConfigDriver;

class InheritanceConfigDriver implements ConfigDriver
{
    protected $driver;
    protected $configPath;

    /**
     * @param $configPath
     */
    public function __construct($configPath)
    {
        $this->configPath = $configPath;
        $this->driver = new YamlConfigDriver();
    }

    public function load($filename)
    {
        $config = $this->driver->load($filename);
        if (isset($config["parent"])) {
            $parentConfig = $this->load($this->configPath."/{$config["parent"]}.yml");
            $config = array_replace_recursive($parentConfig, $config);
        }

        return $config;
    }

    public function supports($filename)
    {
        return $this->driver->supports($filename);
    }
}
